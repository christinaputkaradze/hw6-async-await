const btn = document.querySelector('.btn')
const infoWrapper = document.querySelector('.ip-wrapper')

const urlIp = 'https://api.ipify.org/?format=json';
const urlInfo = 'http://ip-api.com/json/';

async function sendRequest(url){
    let response = await fetch(url);
    let json = await response.json();
    return json;
}

class Info {
    constructor({query, countryCode, country, region, city, regionName}){
        this.query = query;
        this.countryCode = countryCode;
        this.country = country;
        this.region = region;
        this.city = city;
        this.regionName = regionName;
    }

    render(){
        this.info = document.createElement('div');
        this.info.classList.add('info-content');
        this.info.insertAdjacentHTML('afterbegin', `
            <p>IP: ${this.query}</p>
            <p>Код країни: ${this.countryCode}</p>
            <p>Країна: ${this.country}</p>
            <p>Область: ${this.region}</p>
            <p>Місто: ${this.city}</p>
            <p>Назва регіону: ${this.regionName}</p>`)
        infoWrapper.insertAdjacentElement('afterend', this.info)
    }
}

btn.addEventListener('click', async()=>{
    const ipInfo = await sendRequest(urlIp);
    const info = await sendRequest(urlInfo);
    new Info(info).render();
})


